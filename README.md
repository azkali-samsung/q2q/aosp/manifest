# Samsung Q2Q Lineage DT manifest

## Fetching sources

```
mkdir android && cd android
repo init -u https://github.com/LineageOS/android.git -b lineage-20.0 --git-lfs
git clone -b lineage-20.0 https://gitlab.azka.li/samsung/q2q/aosp/manifest .repo/local_manifests
repo sync -j$(nproc) -c
```

## Building

```
source build/envsetup.sh
lunch lineage_q2q-eng
mka
```
